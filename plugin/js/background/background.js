"use strict";

var lead_array = [];
// const vashMagazinAnalyticsLink = "http://myinsights.cc:8080/ads/upload";
const vashMagazinAnalyticsLink = "http://localhost:8080/ads/upload";

// debugger;

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        //console.log(request);
        // debugger;
        switch (request.type) {
            case Message.UPLOAD_IDS:
                upload_ids(request.data);
                return true;
            case Message.RETRIEVE_IDS:
                retrieve_ids( sendResponse);
                return true;
            case VMAPayload.UPLOAD_ADS:
                upload("PUT", vashMagazinAnalyticsLink, request.data, sendResponse);
                return true;
        }
    }
);

function upload_ids(data){
    lead_array = data.ids;
}

function retrieve_ids (callback){
    callback(lead_array);
}

function upload (method, url, data, sendResponse){
    console.log("uploading " + JSON.stringify(data));
    $.ajax({
        type: method,
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),

        success: function (data, textStatus, jqXHR) {
            console.log("upload success. data:", data, " textStatus: ", textStatus, " jqXHR: ", jqXHR);
            sendResponse(data)
        },

        error: function (jqXHR, textStatus, errorThrown) {
            console.log("upload error. jqXHR:", jqXHR, " textStatus: ", textStatus, " errorThrown: ", errorThrown);
            sendResponse(null)
        }
    });
}
