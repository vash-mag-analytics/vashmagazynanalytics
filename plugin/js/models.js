"use strict";
//NOTE: This file lives in both realms: page realm and background realm.

const customerId = "0FC39D7C-D0A8-4152-9230-627AD74A82D6";
const authenticationId = "45E27AFD-10E4-4E06-BC9D-89F17E4AB238";
const idRegexp = /(\d+)/;


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


function uploadCallback (isSuccess) {
    if(isSuccess){
        console.log("success a: ", isSuccess);
        //if page contains button >> we click on it and browser will get us to the next page
        if($(".page > a:contains('»')").get().length>0){
            window.location = $(".page > a:contains('»')").attr("href");
        }
    } else {
        console.log("upload failure a: ", isSuccess);
    }

}

function AdHeadline(j) {
    try {
        this.link = j.find(".go-to-pp").attr("href");
        this.fullLink = new URL(this.link, window.location.href).href;
        this.foundOnLink = window.location.href;
        this.id = idRegexp.exec(this.link)[0];
        this.title = j.find(".ner_h3 > a").text();
        this.roomCount = j.find(".kim > span").text();
        this.material = j.find(".stin").text();
        this.floor = j.find('.ogo-hided')[0].parentElement.childNodes[4].textContent;
        this.space = j.find('.ogo-hided')[1].parentElement.childNodes[4].textContent;
        try {
            this.price = j.find('.price_ner').children()[0].children[2].children[0].innerText;
        } catch (e) {
            console.error("Exception determining price: Title:", this.title,
                // " foundOnLink: ", this.foundOnLink,
                " fullLink: " + this.fullLink);
            // debugger
            this.price = 0;
        }

        this.phone = j.find(".opened-tel").map(function () {
            return this.textContent;
        }).get();
        // this.phone = ["063 54463 22", "093 54463 22"];
        this.timestamp = Date.now();
    } catch (e) {
        debugger
        console.error("Exception Title:", this.title, " foundOnLink: ", this.foundOnLink);
        console.error(e);
    }

    this.toDic = function () {
        return {
            "id": this.id,
            "link": this.link,
            "title": this.title,
            "roomCount": this.roomCount,
            "material": this.material,
            "floor": this.floor,
            "space": this.space,
            "price": this.price,
            "phone": this.phone,
            "timestamp": this.timestamp,
            "fullLink": this.fullLink,
            "foundOnLink": this.foundOnLink
        };
    };

    this.toJSON = function () {
        const data = this.toDic();
        return JSON.stringify(data);
    }
}

function VMAPayload(type, data) {
    return {type: type, data: data};
}

VMAPayload.UPLOAD_ADS = "UPLOAD_ADS";
// VMAPayload.RETRIEVE_IDS = "RETRIEVE_IDS";

async function scrape() {
    if(2==2)  {
        //clicking on all phone numbers
        $(".tell-show-span").trigger("click");
        //waiting till all phone numbers are displayed
        do {
            //count is number of phones that are still hidden, stupid vashmagazin takes 20sec to show all phone numbers
            var count = $(".tell-show-span").length;
            console.log("sleeping, remaining phones to download " + count);
            await sleep(1000);
        } while (count != 0)
    }

    var aa = $(".ogo-buf > tbody").map(function () {
        return new AdHeadline($(this));
    }).get();

    console.log(aa);
    // debugger;
    var dicList = aa.map(function (adHeadline) {
        return adHeadline.toDic();
    });

    var message = new VMAPayload(VMAPayload.UPLOAD_ADS, dicList);
    chrome.runtime.sendMessage(message, uploadCallback);
}

scrape();



// Production API URL is set during plugin compilation. See crxmake/make file for more details.
// Use Mac to build it
const LOANMART_URL = 'https://test.api.800loanmart.com/loanmartservice.svc/restssl/submitapplication';

// Message model for communication between content scripts and background
function Message(type, data) {
    return {type: type, data: data};
}

Message.UPLOAD_IDS = "UPLOAD_IDS";
Message.RETRIEVE_IDS = "RETRIEVE_IDS";




//Class Car
function Car(){
    this.type = $("#cph_control244").val();
    this.vin  =  $("#cph_control210").val();
    this.license_plate = $("#cph_control172").val();
    this.year = $("#cph_control168").val();
    this.make =  $("#cph_control169").val();
    this.model =  $("#cph_control170").val();
    this.style =  $("#cph_control245").val();
    this.mileage =  $("#cph_control171").val();
    this.color =  $("#cph_control246").val();
    this.kbb =  $("#cph_control247").val();
}

//Class Person
function Person() {
    this.id = window.location.href.replace(VELOCIFY_URL + "/Web/LeadAddEdit.aspx?LeadId=", "");
    this.first_name = $("#cph_control4").val();
    this.middle_name = $("#cph_control163").val();
    this.second_name = $("#cph_control5").val();
    this.home_phone =  $("#cph_control11").val();
    this.mobile_phone = $("#cph_control80").val();
    this.dob =  $("#cph_control164").val();
    this.ssn =  $("#cph_control165").val();
    this.address1 = $("#cph_control6").val();
    this.address2 =  $("#cph_control87").val();
    this.city =  $("#cph_control7").val();
    this.state =  $("#cph_control8").val();
    this.zip =  $("#cph_control160").val();
    this.email =  $("#cph_control3txt").val();
}
// Deprecated. Candidate for removal
Person.prototype.get_id = function(){
    window.location.href.replace(VELOCIFY_URL + "Web/LeadAddEdit.aspx?LeadId=", "");
    return 1;
};

//Class LoanMartPayLoad
function LoanMartPayLoad (){

    var person = new Person();
    var car = new Car();
    this.init_data = function() {
        this.data = {
            "customerId": customerId,
            "authenticationId": authenticationId,
            "loanInformation": {
                "AddressInformation": {
                    "AddressLine1": person.address1,
                    "AddressLine2": person.address2,
                    "City": person.city,
                    "State": new State().get_state_id(person.state),
                    "Zip": person.zip
                },
                "CustomerInformation": {
                    "BusinessDisclosure": true,
                    "DOB": person.dob,
                    "Email": person.email,
                    "FirstName": person.first_name,
                    "HomePhone": person.home_phone,
                    "LastName": person.second_name,
                    "MiddleName": person.middle_name,
                    "MobilePhone": person.mobile_phone,
                    "SSN": person.ssn,
                    "State": new State().get_state_id(person.state)
                },
                "ReferralInformation": {
                    "Source": "Partner",
                    "SourceLocation": "Trading Financial Credit",
                    "StoreNumber": "1"
                },
                "VehicleInformation": {
                    "LicensePlate": car.license_plate,
                    "Make": {
                        "MakeDescription": car.make
                    },
                    "Mileage": car.mileage,
                    "Model": {
                        "ModelDescription": car.model
                    },
                    "State": new State().get_state_id(person.state),
                    "Style": {
                        "StyleDescription": car.style
                    },
                    "Year": {
                        "Year": car.year
                    }
                }
            }
        };
    };
    this.sanitize = function(){
        this.data.loanInformation.VehicleInformation.Mileage =
            this.data.loanInformation.VehicleInformation.Mileage.replace(",","");

        if (this.data.loanInformation.VehicleInformation.Style.StyleDescription == "")
            delete this.data.loanInformation.VehicleInformation.Style;

        // Vehicle information is not normalised. Disabling it for now.
        delete this.data.loanInformation.VehicleInformation;

        // See date_unit_test for supported dob options
        var dob = this.fix_date(this.data.loanInformation.CustomerInformation.DOB);

        if (!dob) delete this.data.loanInformation.CustomerInformation.DOB;
        else this.data.loanInformation.CustomerInformation.DOB = dob;
    };

    this.fix_date = function(dob){
        var separator;
        if (dob != "" ){
            if (dob.indexOf("/") > -1) separator = "/";
            if (dob.indexOf("\\") > -1) separator = "\\";
            if (dob.indexOf("-") > -1) separator = "-";
            if (separator) {

                var dob_arr = dob.split(separator);
                if (dob_arr && dob_arr.length == 3){
                    var day, month, year;

                    month = ("0" + dob_arr[1]).slice(-2); // prepend leading 0 if it is just "2"
                    if (!between(month,0, 12)) new UserException("Invalid Date for format YYYY-MM-DD: " +
                    dob);
                    day = dob_arr[2];
                    if (day.length <=2 ){
                        if (between(day, 1, 31)){
                            // last element is day
                            day = ("0" + dob_arr[2]).slice(-2); // prepend leading 0 if it is just "2"
                            year = ("19" + dob_arr[0]).slice(-4);
                            month = ("0" + dob_arr[1]).slice(-2); // prepend leading 0 if it is just "2"
                            if (!between(month,0, 12)) new UserException("Invalid Date for format YYYY-MM-DD: " +
                            dob);
                        }else if (between(day, 32, 2031)){
                            //last element is year
                            month = ("0" + dob_arr[0]).slice(-2); // prepend leading 0 if it is just "2"
                            day = ("0" + dob_arr[1]).slice(-2); // prepend leading 0 if it is just "2"
                            year = ("19" + dob_arr[2]).slice(-4);
                            if (!between(month,0, 12)) new UserException("Invalid Date for format YYYY-MM-DD: " +
                            dob);
                        }
                    }else if (day.length == 4 ){
                        if (between(day, 32, 2031)){
                            //last element is year
                            month = ("0" + dob_arr[0]).slice(-2); // prepend leading 0 if it is just "2"
                            day = ("0" + dob_arr[1]).slice(-2); // prepend leading 0 if it is just "2"
                            year = ("19" + dob_arr[2]).slice(-4);
                            if (!between(month,0, 12)) new UserException("Invalid Date for format YYYY-MM-DD: " +
                            dob);
                        }
                    }
                }
            }
            dob = year + "-" + month + "-" + day;
            var d_ar = dob.split("-");
            if (d_ar.length != 3) throw new UserException("Invalid Date for format YYYY-MM-DD: " + dob);

            var dobInput = new Date(month + "/" + day + "/" + year); //date would be user input example: new Date('MM DD YYYY');
            var dateOfBirth = String(dobInput.getTime()).concat("+0800");
            dob = "\/Date("+dateOfBirth +")\/";
        }else return false;
        console.log(dob);
        return dob
    };

    this.upload = function(success_call_back, error_call_back){
        this.init_data();
        this.sanitize();
        console.log(JSON.stringify(this.data));
        $.ajax({
            type: 'POST',
            url: LOANMART_URL,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(this.data),
            //data: this.data,
            success: function(data, textStatus, jqXHR ){
                success_call_back (data, textStatus, jqXHR)},
            error:  function(jqXHR, textStatus, errorThrown ){
                error_call_back (jqXHR, textStatus, errorThrown)}
        });
    };
    this.go_to_next = function(){
        var message = new Message(Message.RETRIEVE_IDS);
        chrome.runtime.sendMessage(message, function(ids_arr){
            console.log(ids_arr);
            var index = ids_arr.indexOf(get_current_lead_id());
            if (index > -1) {
                ids_arr.splice(index, 1);
            }

            var next_id = ids_arr.shift();
            var data = {
                ids: ids_arr
            };
            var message = new Message(Message.UPLOAD_IDS, data);
            chrome.runtime.sendMessage(message);
            if (next_id) window.location.href= "https://lm2.leads360.com/Web/LeadAddEdit.aspx?LeadId=" + next_id;
        });
    };
    this.init_dialog = function(){
        $("#first_name").val(person.first_name);
        $("#middle_name").val(person.middle_name);
        $("#second_name").val(person.second_name);
        $("#home_phone").val(person.home_phone);
        $("#dob").val(person.dob);
        $("#ssn").val(person.ssn);
        $("#address1").val(person.address1);
        $("#address2").val(person.address2);
        $("#city").val(person.city);
        $("#state").val(person.state);
        $("#zip").val(person.zip);
        $("#email").val(person.email);
///////////////////////////////   CAR        ///////////////////////////////////
        $("#type").val(car.type);
        $("#vin").val(car.vin);
        $("#license_plate").val(car.license_plate);
        $("#year").val(car.year);
        $("#make").val(car.make);
        $("#model").val(car.model);
        $("#style").val(car.style);
        $("#mileage").val(car.mileage);
        $("#color").val(car.color);
        $("#kbb").val(car.kbb);
    }
}

// State Class
function State(){
    this.get_state_id = function(state){
        var wrong_states = ["US-CALIFORNIA","STATECALIFORNIA","STATECALIF","STATECA.","STATE. CA","STATE CA.", "STATE CA","CALIFORNIA","CALIF.","CALIF", "CA."];
        wrong_states.forEach(function(elem){
            elem = elem.toLowerCase();
        });
        var state_lc=state.toLowerCase();
        if (wrong_states.indexOf(state_lc)) state_lc = 'ca';
        for (var i=0; i< this.states.length; i++){
            if (this.states[i][0].toLowerCase() == state_lc)
                return this.states[i][1];
        }
        throw new UserException("Unknown State: " + state);
    }
}

State.prototype.states = [
    ["AL",0],
    ["AK",1],
    ["AZ",2],
    ["AR",3],
    ["CA",4],
    ["CO",5],
    ["CT",6],
    ["DE",7],
    ["DC",8],
    ["FL",9],
    ["GA",10],
    ["HI",11],
    ["ID",12],
    ["IL",13],
    ["IN",14],
    ["IA",15],
    ["KS",16],
    ["KY",17],
    ["LA",18],
    ["ME",19],
    ["MD",20],
    ["MA",21],
    ["MI",22],
    ["MN",23],
    ["MS",24],
    ["MO",25],
    ["MT",26],
    ["NE",27],
    ["NV",28],
    ["NH",29],
    ["NJ",30],
    ["NM",31],
    ["NY",32],
    ["NC",33],
    ["ND",34],
    ["OH",35],
    ["OK",36],
    ["OR",37],
    ["PW",38],
    ["PA",39],
    ["PR",40],
    ["RI",41],
    ["SC",42],
    ["SD",43],
    ["TN",44],
    ["TX",45],
    ["UT",46],
    ["VT",47],
    ["VI",48],
    ["VA",49],
    ["WA",50],
    ["WV",51],
    ["WI",52],
    ["WY",53]
];

function UserException(message) {
    this.message = message;
    this.name = "UserException";
}

function between(x, min, max) {
    var _x = parseInt(x);
    if ((_x + "") != x ) new UserException("Invalid number: " + x);
    return _x >= min && _x <= max;
}

function date_unit_test() {
    if (fix_date("12/1/69")    == "\/Date(-2649600000+0800)\/") console.log("test0 ok");
    if (fix_date("1982-02-06") == "\/Date(381830400000+0800)\/") console.log("test1 ok");
    if (fix_date("04/26/57")   == "\/Date(-400266000000+0800)\/") console.log("test2 ok");
    if (fix_date("05/09/60") == "\/Date(-304448400000+0800)\/") console.log("test3 ok");
    if (fix_date("6/5/74") == "\/Date(139647600000+0800)\/") console.log("test4 ok");
    if (fix_date("12/1/69") == "\/Date(-2649600000+0800)\/") console.log("test5 ok");
    if (fix_date("69/12/1") == "\/Date(-2649600000+0800)\/") console.log("test6 ok");
    if (fix_date("69/2/1") == "\/Date(-28828800000+0800)\/") console.log("test7 ok");
    if (fix_date("1980/2/1") == "\/Date(318240000000+0800)\/") console.log("test8 ok");
    if (fix_date("02/01/1980") == "\/Date(318240000000+0800)\/") console.log("test9 ok");
}
