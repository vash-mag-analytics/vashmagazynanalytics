"use strict";
var lead_array = [];
const VELOCIFY_URL  = window.location.protocol + "//" + window.location.host;

// debugger;

$(document).ready(function() {
  chrome.storage.local.get(["go_to_next"], function (result) {
      return;
    if (result.go_to_next == true) {
      chrome.storage.local.set({"go_to_next": false});
      $("#cph_saveAndGoButton").click();
    }
  });
  //  Adding custom CSS
  $(css).appendTo("head");

  if (window.location.href.indexOf(VELOCIFY_URL + "/Web/LeadAddEdit.aspx?") == 0) {
    //We are on the lead page. Adding Export button

    var $exp_btn = $("<input name='exportButton' type='button' id='exportButton' value='Export to Partner1'>");
    $("#cph_optionsButton").before($exp_btn);

    $exp_btn.click(function () {
      var loan_mart_payload = new LoanMartPayLoad();

      var $dialog = $('<div id="dialog"/>');
      var in_process=false; // is AJax sent to 800loammart?
      var upload_successful = false;
      $dialog.html(html).dialog(
        {
          title: "Export to Partner 1",
          position: {my: "right top", at: "left bottom", of: "#exportButton"},
          maxWidth:700,
          maxHeight: 700,
          width: 700,
          height: 425,
          buttons: {
            "Upload to Partner 1": function() {
              if (in_process) return;
              in_process = true;

              if (upload_successful) {
                chrome.storage.local.set({"go_to_next": true});
                $("#cph_topApplyButton").click();
                return;
              }
              $("body").css('cursor', 'wait');
              $(".ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span").
                attr("disabled", true).addClass("ui-state-disabled");

              // Success handler for Ajax call back
              var succ = function(data, textStatus, jqXHR){
                $("#cph_control105").text($("#cph_control105").
                  text() + "\nUpload to Partner 1 status: success\nApplication ID:" + data.SubmitApplicationResult);
                in_process = false;
                upload_successful = true;
                $("body").css('cursor', 'default');
                $(".ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span").
                  attr("disabled", false).removeClass("ui-state-disabled");
                $(".ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span").
                  text("Save and go to next >>");
                $("#ajax_message").find("> span").text("Export successful").css("color", "#7FFF00").
                  css("background-color", "green");

                var text1 = 'Sent to Partner 1';
                $("#cph_statusDropDownList option").filter(function() {
                    return $(this).text() == text1;
                }).attr('selected', true);
              };
              //  Error handler for Ajax call back
              var err = function(jqXHR, textStatus, errorThrown ){
                in_process = false;
                $("body").css('cursor', 'default');
                $(".ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span").
                  attr("disabled", false).removeClass("ui-state-disabled");

                $("#ajax_message").find("> span").text("Export Failed: " + jqXHR.responseText).css("color", "yellow").
                  css("background-color", "red");
                $("#cph_control105").text($("#cph_control105").text() + "\nUpload to Partner 1 status: failed");
              };
              try {
                loan_mart_payload.upload(succ, err);
              }catch (err){
                $("body").css('cursor', 'default');
                $("#ajax_message").find("> span").text("Export Failed: " + err.message).css("color", "yellow").
                  css("background-color", "red");
              }
            },
            Cancel: function() {
              $("#dialog").remove();
            }
          },
          close: function() {
            $("#dialog").remove();
          }
        }
      );

      loan_mart_payload.init_dialog();

    });
  }
});
//window.location.href= "https://lm.velocify.com/Web/LeadAddEdit.aspx?LeadId=1179746"
//window.location.href= "https://lm.velocify.com/Web/LeadAddEdit.aspx?LeadId=1187833"

function get_current_lead_id (){
  var params_str = window.location.href.replace("https://lm2.leads360.com/Web/LeadAddEdit.aspx?");
  var params_arr = params_str.split('=');
  return params_arr[1];
}

//  HTML for dialog
var html = " \
<div>\
    <div id='ajax_message'><span>&nbsp;</span></div>\
    <div id='left'>\
        <span>Vehicle info:</span>\
        <br>\
        <input disabled type='text' id='first_name' value=''>: First name<br>\
        <input disabled type='text' id='middle_name'>: Middle name<br> \
        <input disabled type='text' id='second_name'>: Second name<br> \
        <input disabled type='text' id='home_phone'>: Home phone<br> \
        <input disabled type='text' id='mobile_phone'>: Mobile phone<br> \
        <input disabled type='text' id='dob'>: Dob<br> \
        <input disabled type='text' id='ssn'>: SSN <br>\
        <input disabled type='text' id='address1'>: Address1<br>\
        <input disabled type='text' id='address2'>: Address2<br>\
        <input disabled type='text' id='city'>: City <br>        \
        <input disabled type='text' id='state'>: State <br>      \
        <input disabled type='text' id='zip'>: Zip  <br>        \
        <input disabled type='text' id='email'>: Email<br>       \
    </div> \
    <div id='right'\
        <span>Vehicle info:</span>\
        <br>\
        <input disabled type='text' id='type'>: Type<br>         \
        <input disabled type='text' id='vin'>: Vin<br>           \
        <input disabled type='text' id='license_plate'>: License plate <br>   \
        <input disabled type='text' id='year'>: Year<br>        \
        <input disabled type='text' id='make'>: Make:<br>         \
        <input disabled type='text' id='model'>: Model<br>       \
        <input disabled type='text' id='style'>: Style<br>       \
        <input disabled type='text' id='mileage'>: Mileage<br>   \
        <input disabled type='text' id='color'>: Color<br>       \
        <input disabled type='text' id='kbb'>: KBB<br>           \
    </div> \
</div>";


// Custom CSS for the dialog
var css = "<style type='text/css'> div#right input, div#left input { background-color:white;} </style>";
