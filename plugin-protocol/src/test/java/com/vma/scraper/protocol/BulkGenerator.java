package com.vma.scraper.protocol;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Copyright (c) 2019, Twilio Inc.
 *
 * @author Ihor Yuvchenko
 */

/**
 *     <dependency>
 *       <groupId>com.vma</groupId>
 *       <artifactId>plugin-protocol</artifactId>
 *       <classifier>tests</classifier>
 *       <type>test-jar</type>
 *       <version>1.0-SNAPSHOT</version>
 *       <scope>test</scope>
 *     </dependency>
 */
public class BulkGenerator {
    public final static long DAY_IN_MS = 1000 * 60 * 60 * 24;
    public final static long CURRENT_TIME_MILLIS = System.currentTimeMillis();
    public final static long FIVE_DAYS_AGO = CURRENT_TIME_MILLIS - (5 * DAY_IN_MS);
    public final static long SEVEN_DAYS_AGO = CURRENT_TIME_MILLIS - (7 * DAY_IN_MS);



    public static List<AdHeadline> generate6MonthOfData() {
        int numberOfDays = 120;
        int start = 70_000;
        int end = start + 10_000;

        return IntStream.range(0, numberOfDays).mapToLong(day -> CURRENT_TIME_MILLIS - (day * DAY_IN_MS))
            .mapToObj(dayTimeStamp -> IntStream.range(start, end)
                .mapToObj(i ->
                    new AdHeadline(String.valueOf(i), "link",
                        "Prodam 1-kim kvartury",
                        "3",
                        "Tsegla",
                        "4/5",
                        "60/05/100",
                        "$100",
                        new String[]{"+1415-555-4535"},
                        String.valueOf(dayTimeStamp),
                        "www.example.com", "www.example.com/qwerty?page=2")
                ).collect(Collectors.toList()))
            .collect(Collector.of(ArrayList::new, List::addAll, (a, b) -> a));

    }


    public static List<AdHeadline> generateBulkWithChangeTitle() {

        int start = 70_000;
        int end = start + 10_000;
        final List<AdHeadline> sevenDaysAgoList = IntStream.range(start, end).mapToObj(i ->
            new AdHeadline(String.valueOf(i), "link",
                "Prodam 1-kim kvartury",
                "3",
                "Tsegla",
                "4/5",
                "60/05/100",
                "$100",
                new String[]{"+1415-555-4535"},
                String.valueOf(SEVEN_DAYS_AGO),
                "www.example.com", "www.example.com/qwerty?page=2")
        ).collect(Collectors.toList());

        int mid = (end - start) / 2;

        final List<AdHeadline> oneChanged = IntStream.range(start, end).mapToObj(i ->
            new AdHeadline(String.valueOf(i), "link",
                i == mid ? "Prodam 1-kim budynok" : "Prodam 1-kim kvartury",
                "3",
                "Tsegla",
                "4/5",
                "60/05/100",
                "$100",
                new String[]{"+1415-555-4535"},
                String.valueOf(FIVE_DAYS_AGO),
                "www.example.com",  "www.example.com/qwerty?page=2")
        ).collect(Collectors.toList());

        final List<AdHeadline> currentTimeList = IntStream.range(start, end).mapToObj(i ->
            new AdHeadline(String.valueOf(i), "link",
                "Prodam 1-kim kvartury",
                "3",
                "Tsegla",
                "4/5",
                "60/05/100",
                "$100",
                new String[]{"+1415-555-4535"},
                String.valueOf(SEVEN_DAYS_AGO),
                "www.example.com", "www.example.com/qwerty?page=2")
        ).collect(Collectors.toList());

        sevenDaysAgoList.addAll(oneChanged);
        sevenDaysAgoList.addAll(currentTimeList);

        return sevenDaysAgoList;
    }

    public static void main(String[] args) {
        List<AdHeadline> l = generateBulkWithChangeTitle();
        System.out.println(l);
        l = generate6MonthOfData();
        System.out.println(l);
    }
}