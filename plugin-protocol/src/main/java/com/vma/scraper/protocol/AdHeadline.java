package com.vma.scraper.protocol;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * @author Ihor Yuvchenko
 */

//@JsonIgnoreProperties(ignoreUnknown = true)
public class AdHeadline {
    @JsonProperty
    private final String id;
    @JsonProperty
    private final String link;
    @JsonProperty
    private final String title;
    @JsonProperty
    private final String roomCount;
    @JsonProperty
    private final String material;
    @JsonProperty
    private final String floor;
    @JsonProperty
    private final String space;
    @JsonProperty
    private final String price;
    @JsonProperty
    private final String [] phone;
    @JsonProperty
    private final String timestamp;
    @JsonProperty
    private final String fullLink;
    private final String foundOnLink;



    @JsonCreator
    public AdHeadline(@NotNull @JsonProperty("id") final String id,
                      @JsonProperty("link") final String link,
                      @JsonProperty("title") final String title,
                      @JsonProperty("roomCount") final String roomCount,
                      @JsonProperty("material") final String material,
                      @JsonProperty("floor") final String floor,
                      @JsonProperty("space") final String space,
                      @JsonProperty("price") final String price,
                      @JsonProperty("phone") final String[] phone,
                      @JsonProperty("timestamp") final String timestamp,
                      @JsonProperty("fullLink") final String fullLink,
                      @JsonProperty("foundOnLink") String foundOnLink) {
        this.id = id;
        this.link = link;
        this.title = title;
        this.roomCount = roomCount;
        this.material = material;
        this.floor = floor;
        this.space = space;
        this.price = price;
        this.phone = phone;
        this.timestamp = timestamp;
        this.fullLink = fullLink;
        this.foundOnLink = foundOnLink;
    }

    public String getId() {
        return id;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getRoomCount() {
        return roomCount;
    }

    public String getMaterial() {
        return material;
    }

    public String getFloor() {
        return floor;
    }

    public String getSpace() {
        return space;
    }

    public String getPrice() {
        return price;
    }

    public String [] getPhone() {
        return phone;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getFullLink() {
        return fullLink;
    }

    public String getFoundOnLink() {
        return foundOnLink;
    }

    private static final DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final ZoneId utcZoneId = ZoneId.of("UTC");

    public String formattedDate() {
        Instant instant = Instant.ofEpochMilli(new Long(timestamp));
        ZonedDateTime utcTime = instant.atZone(utcZoneId);
        return utcTime.format(formatters);
    }

    @Override
    public String toString() {
        return "AdHeadline{" +
            "id='" + id + '\'' +
            ", link='" + link + '\'' +
            ", title='" + title + '\'' +
            ", roomCount='" + roomCount + '\'' +
            ", material='" + material + '\'' +
            ", floor='" + floor + '\'' +
            ", space='" + space + '\'' +
            ", price='" + price + '\'' +
            ", phone=" + Arrays.toString(phone) +
            ", timestamp='" + timestamp + '\'' +
            ", fullLink='" + fullLink + '\'' +
            ", foundOnLink='" + foundOnLink + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdHeadline)) return false;

        AdHeadline that = (AdHeadline) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (link != null ? !link.equals(that.link) : that.link != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (roomCount != null ? !roomCount.equals(that.roomCount) : that.roomCount != null) return false;
        if (material != null ? !material.equals(that.material) : that.material != null) return false;
        if (floor != null ? !floor.equals(that.floor) : that.floor != null) return false;
        if (space != null ? !space.equals(that.space) : that.space != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(phone, that.phone)) return false;
        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
        if (fullLink != null ? !fullLink.equals(that.fullLink) : that.fullLink != null) return false;
        return foundOnLink != null ? foundOnLink.equals(that.foundOnLink) : that.foundOnLink == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (roomCount != null ? roomCount.hashCode() : 0);
        result = 31 * result + (material != null ? material.hashCode() : 0);
        result = 31 * result + (floor != null ? floor.hashCode() : 0);
        result = 31 * result + (space != null ? space.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(phone);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (fullLink != null ? fullLink.hashCode() : 0);
        result = 31 * result + (foundOnLink != null ? foundOnLink.hashCode() : 0);
        return result;
    }
}
