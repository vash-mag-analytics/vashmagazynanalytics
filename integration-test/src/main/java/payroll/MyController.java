package payroll;

import com.vma.scraper.protocol.AdHeadline;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//@RestController
@RestController
@RequestMapping("ads")
public class MyController {

    @GetMapping("/hello")
    public Collection<String> sayHello() {
        return IntStream.range(0, 10)
            .mapToObj(i -> "Hello nufmber " + i)
            .collect(Collectors.toList());
    }

    @GetMapping("/ad")
    public Collection<AdHeadline> adGet() {
        AdHeadline myAdHeadline = new AdHeadline("76677", "link", "Prodam 1-kim kvartury", "3",
            "Tsegla", "4/5", "60/05/100", "$100",
            new String[]{"+1415-555-4535"}, "" + System.currentTimeMillis(),
            "www.example.com", "www.example.com/qwerty?page=2");
        return Collections.singleton(myAdHeadline);
    }

    @PutMapping(path = "/upload", consumes = "application/json", produces = "application/json")
    public Collection<AdHeadline> put3(@RequestBody List<AdHeadline> adHeadlines) {
        adHeadlines.forEach(ad -> System.out.println("ad ======>"+ad));
        return adHeadlines;
    }

    @PutMapping(path = "/ad/{id}", consumes = "application/json", produces = "application/json")
    public Collection<AdHeadline> put(@PathVariable("id") long id,
                                      @RequestBody AdHeadline adHeadline) {
        System.out.println(" ====dddaaa===" + id + " ======= " + adHeadline);
        return Collections.singleton(adHeadline);
    }
}